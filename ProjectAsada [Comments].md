[Best Read in a Markdown Viewer.]

Soo this is basically what I have done in terms of the actual model.
I hoped to do more but I have to submit this check first, so yeah.

What's so different about this Mesh from others is that, It uses a custom MeshInstruction set.

```
Mesh::addInstruction
Mesh::ExecuteInstructions
```

Here's a snippet of how to draw a shape

```cpp
workingmesh = MeshBuilder::Hemisphere("HairBase", 0.0f, 0.0f, 0.0f, 0.5f, 0x423A37); // Hemisphere
workingmesh->addInstruction(ExInstructionTypes::PSHMATRX);                           // Push the Matrix
workingmesh->addInstruction(ExInstructionTypes::TRANSLATE, 0.0f, 0.0f, -0.4f, 0.0f); // Translate it downwards by -0.4f
workingmesh->addInstruction(ExInstructionTypes::RENDER);                             // Render it.
meshList.push_back(workingmesh);                                                     // To finalise the mesh
```

Although it would be easier to use your Vertex.h header, I prefer doing this method.

There is still a long list of things to do...  
such as:
- Detailing hair [Will be last.]
- Sniper Gun Model [Will be worked on separately.]
- Lower body
- Arms
- Legs
- Feet

All are drawn with primitives [as expected]

- A bit of the hair detailing is done, but I'm still not happy with the result. probably will have to rethink how I actually will draw the hair.  
  Those uses a Octahedron aka a prism(?)
- Hair is using an uncapped hemisphere [Which I do need to shrink it... coming to think of it...]
- Rest of the parts uses:  
  sliced A Torus  
  Stretched Cylinder  
  Ellipsoid [Or basically a sphere.]

That's all for me I guess.

~ Shinon

**P.S.** Live [About as live as it can get]  
         source code can be found here: https://gitlab.com/Ristellise/cegl
     
**P.P.S** Why Project Asada? Because it's Asada Shinon I'm doing from Sword Art Online  
          and also It's the first project folder I create for this semester on my Hard disk. so Project A,B C... So it's A -> Asada.
CONTROLS:

WASD [Forward, Backward, Strafe left, Strafe right]

Arrow Keys [↑ ↓ ← →] [Look up,Look down,Look Left,Look Right]

Shift, CTRL [Ascend, Descend]

NOTE: Requires a prebuilt `glew32.dll`.  
A copy can be found here: https://gitlab.com/Ristellise/regl/blob/master/Debug/glew32.dll
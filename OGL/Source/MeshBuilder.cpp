#include "MeshBuilder.h"
#include <GL\glew.h>
#include <iostream>
#include "MeshGenerators.h"
#include <MyMath.h>
/******************************************************************************/
/*!
\brief
Generate the vertices of a reference Axes; Use red for x-axis, green for y-axis, blue for z-axis
Then generate the VBO/IBO and store them in Mesh object

\param meshName - name of mesh
\param lengthX - x-axis should start at -lengthX / 2 and end at lengthX / 2
\param lengthY - y-axis should start at -lengthY / 2 and end at lengthY / 2
\param lengthZ - z-axis should start at -lengthZ / 2 and end at lengthZ / 2

\return Pointer to mesh storing VBO/IBO of reference axes
*/
/******************************************************************************/
Mesh* MeshBuilder::GenerateAxes(const std::string &meshName, float lengthX, float lengthY, float lengthZ)
{
    RBuffers *res = new RBuffers;
    add_vertex(-100.0, 0.0, 0.0, res->vectorbuffer, res->colorbuffer, 0xff0000, true, res->normalbuffer);
    add_index(0, res->indexbuffer);
    add_vertex(100.0, 0.0, 0.0, res->vectorbuffer, res->colorbuffer, 0xff0000, true, res->normalbuffer);
    add_index(1, res->indexbuffer);

    add_vertex(0.0, -100.0, 0.0, res->vectorbuffer, res->colorbuffer, 0x00ff00, true, res->normalbuffer);
    add_index(2, res->indexbuffer);
    add_vertex(0.0, 100.0, 0.0, res->vectorbuffer, res->colorbuffer, 0x00ff00, true, res->normalbuffer);
    add_index(3, res->indexbuffer);

    add_vertex(0.0, 0.0, -100.0, res->vectorbuffer, res->colorbuffer, 0x0000ff, true, res->normalbuffer);
    add_index(4, res->indexbuffer);
    add_vertex(0.0, 0.0, 100.0, res->vectorbuffer, res->colorbuffer, 0x0000ff, true, res->normalbuffer);
    add_index(5, res->indexbuffer);


    Mesh *mesh = new Mesh(meshName, GL_LINES);
    mesh->indexSize = res->indexbuffer.size();
    glBindBuffer(GL_ARRAY_BUFFER, mesh->vertexBuffer);
    // Transfer vertices to OpenGL
    glBufferData(GL_ARRAY_BUFFER, res->vectorbuffer.size() * sizeof(float), &res->vectorbuffer[0],
        GL_STATIC_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER, mesh->colorBuffer);
    glBufferData(GL_ARRAY_BUFFER, res->colorbuffer.size() * sizeof(float), &res->colorbuffer[0],
        GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mesh->indexBuffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, res->indexbuffer.size() * sizeof(unsigned int),
        &res->indexbuffer[0], GL_STATIC_DRAW);
    return mesh;
}

/******************************************************************************/
/*!
\brief
Generate the vertices of a quad; Use random color for each vertex
Then generate the VBO/IBO and store them in Mesh object

\param meshName - name of mesh
\param lengthX - width of quad
\param lengthY - height of quad

\return Pointer to mesh storing VBO/IBO of quad
*/
/******************************************************************************/
Mesh* MeshBuilder::GenerateQuad(const std::string &meshName, float lengthX, float lengthY)
{
	const GLfloat vertex_buffer_data[] = { 0
	};
	const GLfloat color_buffer_data[] = { 0
	};
	const GLuint index_buffer_data[] = { 0
	};
	Mesh *mesh = new Mesh(meshName);

	return mesh;
}

/******************************************************************************/
/*!
\brief
Generate the vertices of a cube; Use random color for each vertex
Then generate the VBO/IBO and store them in Mesh object

\param meshName - name of mesh
\param lengthX - width of cube
\param lengthY - height of cube
\param lengthZ - depth of cube

\return Pointer to mesh storing VBO/IBO of cube
*/
/******************************************************************************/
Mesh* MeshBuilder::GenerateCube(const std::string &meshName, float lengthX, float lengthY, float lengthZ)
{
    const GLfloat vertex_buffer_data[] = {
        // front
    -lengthX, -lengthY,  lengthZ,
	 lengthX, -lengthY,  lengthZ,
	 lengthX,  lengthY,  lengthZ,
    -lengthX,  lengthY,  lengthZ,
    // back
    -lengthX, -lengthY, -lengthZ,
	 lengthX, -lengthY, -lengthZ,
	 lengthX,  lengthY, -lengthZ,
    -lengthX,  lengthY, -lengthZ,
    };
	const GLfloat color_buffer_data[] = {
    1.0f, 0.0f, 0.0f, //color of vertex 0
    0.0f, 1.0f, 0.0f, //color of vertex 1
    0.0f, 0.0f, 1.0f, //color of vertex 3

    1.0f, 1.0f, 0.0f, //color of vertex 0
    0.0f, 1.0f, 1.0f, //color of vertex 1
    1.0f, 0.0f, 1.0f, //color of vertex 3

    0.0f, 0.0f, 0.0f, //color of vertex 0
    1.0f, 1.0f, 1.0f, //color of vertex 1
    };
    const GLuint index_buffer_data[] = {
        0, 1, 2,
        2, 3, 0,
        1, 5, 6,
        6, 2, 1,
        7, 6, 5,
        5, 4, 7,
        4, 0, 3,
        3, 7, 4,
        4, 5, 1,
        1, 0, 4,
        3, 2, 6,
        6, 7, 3,
    };
	Mesh *mesh = new Mesh(meshName);
	mesh->indexSize = 36;
    glBindBuffer(GL_ARRAY_BUFFER, mesh->vertexBuffer);
    // Transfer vertices to OpenGL
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertex_buffer_data), vertex_buffer_data,
        GL_STATIC_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER, mesh->colorBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(color_buffer_data), color_buffer_data,
        GL_STATIC_DRAW);
    // ---------------------------- IndexBuffer
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mesh->indexBuffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(index_buffer_data),
        index_buffer_data, GL_STATIC_DRAW);
	return mesh;
}
/*
PolyBuilder:
Builds a polygon at the specified coordinates. Returns a mesh.
*/
Mesh* MeshBuilder::PolyBuilder(
    const std::string &meshName,
    float x,float y,float z,float R,
    int Sides,
    unsigned int InnerColor,unsigned int OuterColor)
{
    // Generate Buffers
    RBuffers* buff = MeshGen::MultiPoly(x, y, z, R, Sides, InnerColor, OuterColor);
	Mesh *mesh = new Mesh(meshName,GL_TRIANGLE_FAN);
	mesh->indexSize = buff->indexbuffer.size();
	glBindBuffer(GL_ARRAY_BUFFER, mesh->vertexBuffer);
	// Transfer vertices to OpenGL
	glBufferData(GL_ARRAY_BUFFER, buff->vectorbuffer.size() * sizeof(float), &buff->vectorbuffer[0],
		GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, mesh->colorBuffer);
	glBufferData(GL_ARRAY_BUFFER, buff->colorbuffer.size() * sizeof(float), &buff->colorbuffer[0],
		GL_STATIC_DRAW);
	// ---------------------------- IndexBuffer
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mesh->indexBuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, buff->indexbuffer.size() * sizeof(unsigned int),
		&buff->indexbuffer[0], GL_STATIC_DRAW);

	return mesh;
}

Mesh* MeshBuilder::Octahedron(
	const std::string &meshName,
	float h, float R,
	unsigned int Color)
{
	// Generate Buffers
	RBuffers* buff = MeshGen::Octahedron(h, R, Color);
	Mesh *mesh = new Mesh(meshName, GL_TRIANGLE_FAN);
	mesh->indexSize = buff->indexbuffer.size();
	glBindBuffer(GL_ARRAY_BUFFER, mesh->vertexBuffer);
	// Transfer vertices to OpenGL
	glBufferData(GL_ARRAY_BUFFER, buff->vectorbuffer.size() * sizeof(float), &buff->vectorbuffer[0],
		GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, mesh->colorBuffer);
	glBufferData(GL_ARRAY_BUFFER, buff->colorbuffer.size() * sizeof(float), &buff->colorbuffer[0],
		GL_STATIC_DRAW);
	// ---------------------------- IndexBuffer
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mesh->indexBuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, buff->indexbuffer.size() * sizeof(unsigned int),
		&buff->indexbuffer[0], GL_STATIC_DRAW);
	
	glBindBuffer(GL_ARRAY_BUFFER, mesh->normalBuffer);
	// Transfer vertices to OpenGL
	glBufferData(GL_ARRAY_BUFFER, buff->normalbuffer.size() * sizeof(float), &buff->normalbuffer[0],
		GL_STATIC_DRAW);

	return mesh;
}

Mesh* MeshBuilder::GenerateCylinder(const std::string &meshName, unsigned numStack, unsigned numSlice, float radius, float height, unsigned int color)
{
    RBuffers* res = new RBuffers;
    float degreePerStack = 180.0f / numStack;
    float degreePerSlice = 360.0f / numSlice;
    float stackHeight = height / numStack;
    for (unsigned stack = 0; stack < numStack + 1; ++stack)
    {
        for (unsigned slice = 0; slice < numSlice + 1; ++slice)
        {
            float theta = slice * degreePerSlice;
            add_vertex(
                radius * cos(Math::DegreeToRadian(theta)),
                -height / 2 + stack * stackHeight,
                radius * sin(Math::DegreeToRadian(theta)),
                res->vectorbuffer, res->colorbuffer, color);
        }
    }

    for (unsigned stack = 0; stack < numStack; ++stack)
    {
        for (unsigned slice = 0; slice < numSlice + 1; ++slice)
        {
            add_index(stack * (numSlice + 1) + slice, res->indexbuffer);
            add_index((stack + 1) * (numSlice + 1) + slice, res->indexbuffer);
        }

    }
    Mesh *mesh = new Mesh(meshName, GL_TRIANGLE_STRIP);
    mesh->indexSize = res->indexbuffer.size();
    glBindBuffer(GL_ARRAY_BUFFER, mesh->vertexBuffer);
    // Transfer vertices to OpenGL
    glBufferData(GL_ARRAY_BUFFER, res->vectorbuffer.size() * sizeof(float), &res->vectorbuffer[0],
        GL_STATIC_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER, mesh->colorBuffer);
    glBufferData(GL_ARRAY_BUFFER, res->colorbuffer.size() * sizeof(float), &res->colorbuffer[0],
        GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mesh->indexBuffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, res->indexbuffer.size() * sizeof(unsigned int),
        &res->indexbuffer[0], GL_STATIC_DRAW);

    return mesh;
}

Mesh* MeshBuilder::Ring(const std::string &meshName, float RingWidth, float RingRadius,float x, float y, float z)
{
    RBuffers *res = new RBuffers;
    add_vertex(x, y, z, res->vectorbuffer, res->colorbuffer, 0x000000);
    int count = 0;
    for (size_t i = 0; i < 100; i++)
    {
        add_vertex(
            x + (RingRadius * cos(i * Math::TWO_PI / 100)),
            y + (RingRadius * sin(i * Math::TWO_PI / 100)),
            z, res->vectorbuffer, res->colorbuffer, 0x000000);
        add_index(count++, res->indexbuffer);
        add_vertex(
            x + ((RingRadius + RingWidth) * cos(i * Math::TWO_PI / 100)),
            y + ((RingRadius + RingWidth) * sin(i * Math::TWO_PI / 100)),
            z, res->vectorbuffer, res->colorbuffer, 0xffffff);
        add_index(count++, res->indexbuffer);
    }
    // Clean up.
    
    //add_index(3, res->indexbuffer);
    add_index(count++, res->indexbuffer);
    add_index(1, res->indexbuffer); add_index(2, res->indexbuffer);
    Mesh *mesh = new Mesh(meshName, GL_TRIANGLE_STRIP);
    mesh->indexSize = res->indexbuffer.size();
    glBindBuffer(GL_ARRAY_BUFFER, mesh->vertexBuffer);
    // Transfer vertices to OpenGL
    glBufferData(GL_ARRAY_BUFFER, res->vectorbuffer.size() * sizeof(float), &res->vectorbuffer[0],
        GL_STATIC_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER, mesh->colorBuffer);
    glBufferData(GL_ARRAY_BUFFER, res->colorbuffer.size() * sizeof(float), &res->colorbuffer[0],
        GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mesh->indexBuffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, res->indexbuffer.size() * sizeof(unsigned int),
        &res->indexbuffer[0], GL_STATIC_DRAW);
    return mesh;
}

Mesh* MeshBuilder::GenerateTorus(const std::string &meshName, unsigned int numStack, unsigned int numSlice, float outerR, float innerR,unsigned long int color,float slice,float stack)
{
	RBuffers *res = new RBuffers;

	float degreePerStack = slice / numStack;
	float degreePerSlice = stack / numSlice;
	float x1, z1;
	float x2, y2, z2;

	for (unsigned stack = 0; stack < numStack + 1; stack++)
	{
		for (unsigned slice = 0; slice < numSlice + 1; slice++)
		{
			z1 = outerR * cos(Math::DegreeToRadian(stack * degreePerStack));
			x1 = outerR * sin(Math::DegreeToRadian(stack * degreePerStack));
			x2 = (outerR + innerR * cos(Math::DegreeToRadian(slice * degreePerSlice))) * cos(Math::DegreeToRadian(stack * degreePerStack));
			y2 = (outerR + innerR * cos(Math::DegreeToRadian(slice * degreePerSlice))) * sin(Math::DegreeToRadian(stack * degreePerStack));
			z2 = innerR * sin(Math::DegreeToRadian(slice * degreePerSlice));
			add_vertex(x2, y2, z2, res->vectorbuffer, res->colorbuffer, color);
		}
	}
	for (unsigned stack = 0; stack < numStack; stack++)
	{
		for (unsigned slice = 0; slice < numSlice + 1; slice++)
		{
			add_index((numSlice + 1) * stack + slice + 0, res->indexbuffer);
			add_index((numSlice + 1) * (stack + 1) + slice + 0, res->indexbuffer);
		}
	}
	Mesh *mesh = new Mesh(meshName, GL_TRIANGLE_STRIP);
	mesh->indexSize = res->indexbuffer.size();
	glBindBuffer(GL_ARRAY_BUFFER, mesh->vertexBuffer);
	// Transfer vertices to OpenGL
	glBufferData(GL_ARRAY_BUFFER, res->vectorbuffer.size() * sizeof(float), &res->vectorbuffer[0],
		GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, mesh->colorBuffer);
	glBufferData(GL_ARRAY_BUFFER, res->colorbuffer.size() * sizeof(float), &res->colorbuffer[0],
		GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mesh->indexBuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, res->indexbuffer.size() * sizeof(unsigned int),
		&res->indexbuffer[0], GL_STATIC_DRAW);
	return mesh;

	return mesh;
}

Mesh* MeshBuilder::Ellipsoid(const std::string &meshName, unsigned int uiStacks, unsigned int uiSlices, float fA, float fB, float fC,unsigned int Color)
{
    RBuffers *res = new RBuffers;
    float Pi = 3.14159265f;
    float tStep = (Pi) / (float)uiSlices;
    float sStep = (Pi) / (float)uiStacks;
    unsigned int ccount = 0;
    for (float t = -Pi / 2; t <= (Pi / 2) + .0001; t += tStep)
    {
        for (float s = -Pi; s <= Pi + .0001; s += sStep)
        {
            add_vertex(fA * cos(t + tStep) * cos(s), fB * cos(t + tStep) * sin(s), fC * sin(t + tStep), res->vectorbuffer, res->colorbuffer, Color);
            add_index(ccount++, res->indexbuffer);
            add_vertex(fA * cos(t) * cos(s), fB * cos(t) * sin(s), fC * sin(t),res->vectorbuffer,res->colorbuffer, Color);
            add_index(ccount++, res->indexbuffer);
            
        }
    }
    //add_index(ccount++, res->indexbuffer);
    Mesh *mesh = new Mesh(meshName, GL_TRIANGLE_STRIP);
    mesh->indexSize = res->indexbuffer.size();
    glBindBuffer(GL_ARRAY_BUFFER, mesh->vertexBuffer);
    // Transfer vertices to OpenGL
    glBufferData(GL_ARRAY_BUFFER, res->vectorbuffer.size() * sizeof(float), &res->vectorbuffer[0],
        GL_STATIC_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER, mesh->colorBuffer);
    glBufferData(GL_ARRAY_BUFFER, res->colorbuffer.size() * sizeof(float), &res->colorbuffer[0],
        GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mesh->indexBuffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, res->indexbuffer.size() * sizeof(unsigned int),
        &res->indexbuffer[0], GL_STATIC_DRAW);
    return mesh;
}

Mesh* MeshBuilder::Hemisphere(const std::string &meshName,
    float x, float y, float z, float R, unsigned int Color)
{
    // Generate HemiSphere
    RBuffers* res = MeshGen::Hemisphere(x, y, z, R, Color);
    Mesh *mesh = new Mesh("This.HemiSphere", GL_TRIANGLE_STRIP);
    mesh->indexSize = res->indexbuffer.size();
    glBindBuffer(GL_ARRAY_BUFFER, mesh->vertexBuffer);
    // Transfer vertices to OpenGL
    glBufferData(GL_ARRAY_BUFFER, res->vectorbuffer.size() * sizeof(float), &res->vectorbuffer[0],
        GL_STATIC_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER, mesh->colorBuffer);
    glBufferData(GL_ARRAY_BUFFER, res->colorbuffer.size() * sizeof(float), &res->colorbuffer[0],
        GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mesh->indexBuffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, res->indexbuffer.size() * sizeof(unsigned int),
        &res->indexbuffer[0], GL_STATIC_DRAW);
    
	return mesh;
}
/*
Mesh Hacks used:
Merging of Vertices into single buffer.
Specify the length for them. and draw only that for the current mode.
Change the mode, and then draw the next set.
Still technically counts as a single mesh... right?
~ Asada Shinon
*/

/*
Adds a Mesh to a buffer list
*/
void Finalize(Mesh *mesh)
{

}

std::vector<Buffers>* MultiMesh(std::vector<Buffers> *BuffersList, RBuffers *mesh2)
{
    std::cout << "Merging RBuffer into BuffersList..." << std::endl;
    Buffers Z;
    Z.vertexBuffer = 1;
    Z.colorBuffer = 2;
    Z.indexBuffer = 3;
    Z.indexSize = mesh2->indexbuffer.size();
    glBindBuffer(GL_ARRAY_BUFFER, Z.vertexBuffer);
    // Transfer vertices to OpenGL
    glBufferData(GL_ARRAY_BUFFER, mesh2->vectorbuffer.size() * sizeof(float), &mesh2->vectorbuffer[0],
        GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, Z.colorBuffer);
    glBufferData(GL_ARRAY_BUFFER, mesh2->colorbuffer.size() * sizeof(float), &mesh2->colorbuffer[0],
        GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, Z.indexBuffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, mesh2->indexbuffer.size() * sizeof(unsigned int),
        &mesh2->indexbuffer[0], GL_STATIC_DRAW);
    BuffersList->push_back(Z);
    return BuffersList;
}
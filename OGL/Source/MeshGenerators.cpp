#include "MeshGenerators.h"
#include <MyMath.h>
/*
Func: add_vertex
- Adds a Vertex & Color to 2 vectors
Returns:
- Void [Vertices are added to Vectors.]
*/

std::vector<float> fkdummfunction()
{
    return std::vector<float>();
}

void add_vertex(float x, float y, float z, std::vector<float> &vertvector, std::vector<float> &colorvector, unsigned long int hexcolor,
				bool addNormal, std::vector<float> &normvector)
{
	vertvector.push_back(x);
	vertvector.push_back(y);
	vertvector.push_back(z);
	colorvector.push_back(((hexcolor >> 16) & 0xFF) / 255.0f);
	colorvector.push_back(((hexcolor >> 8) & 0xFF) / 255.0f);
	colorvector.push_back(((hexcolor) & 0xFF) / 255.0f);
	if (addNormal)
	{
		normvector.push_back(x);
		normvector.push_back(y);
		normvector.push_back(z);
	}
}

void add_normal(float x, float y, float z, std::vector<float> &normvector)
{
	normvector.push_back(x);
	normvector.push_back(y);
	normvector.push_back(z);
}

/*
Func: add_vertex
- Adds a Index to
Returns:
- Void [Vertices are added to Vectors.]
*/
void add_index(unsigned int vectice1, unsigned int vectice2, unsigned int vectice3, std::vector<unsigned int>& indexBuffer)
{
    indexBuffer.push_back(vectice1);
    indexBuffer.push_back(vectice2);
    indexBuffer.push_back(vectice3);
}
void add_index(unsigned int vectice1, std::vector<unsigned int>& indexBuffer)
{
    indexBuffer.push_back(vectice1);
}

RBuffers* MeshGen::MultiPoly(float x, float y, float z, float R,
    int Sides,
    unsigned int InnerColor, unsigned int OuterColor)
{
    RBuffers *res = new RBuffers;
    add_vertex(x, y, z, res->vectorbuffer, res->colorbuffer, 0x000000);
    int count = 0;
    int accuracy = Sides; // Number of Sided polygon. You can make this a circle if the side count is high enough.
    for (size_t i = 0; i < Sides; i++)
    {
        add_vertex(
            x + (R * cos(i * Math::TWO_PI / Sides)),
            y + (R * sin(i * Math::TWO_PI / Sides)),
            z, res->vectorbuffer, res->colorbuffer, OuterColor);
        add_index(count++, res->indexbuffer);
    }
    // Clean up.
    add_index(count++, res->indexbuffer);
    add_index(1, res->indexbuffer);
    return res;
}
RBuffers* MeshGen::Octahedron(float height, float radius, unsigned int Color)
{
    RBuffers* buff = new RBuffers;
    add_vertex(radius, 0,0,buff->vectorbuffer,buff->colorbuffer,Color,true ,buff->normalbuffer);
    add_vertex(-radius, 0,0,buff->vectorbuffer,buff->colorbuffer,Color, true, buff->normalbuffer);
    add_vertex(0, height,0,buff->vectorbuffer,buff->colorbuffer,Color, true, buff->normalbuffer);
    add_vertex(0, -height,0,buff->vectorbuffer,buff->colorbuffer,Color, true, buff->normalbuffer);
    add_vertex(0, 0, radius,buff->vectorbuffer,buff->colorbuffer,Color, true, buff->normalbuffer);
    add_vertex(0, 0, -radius,buff->vectorbuffer,buff->colorbuffer,Color, true, buff->normalbuffer);
    add_index(0, 2, 4, buff->indexbuffer);
    add_index(0, 4, 3, buff->indexbuffer);
    add_index(0, 3, 5, buff->indexbuffer);
    add_index(0, 5, 2, buff->indexbuffer);
    add_index(1, 4, 2, buff->indexbuffer);
    add_index(1, 2, 5, buff->indexbuffer);
    add_index(1, 5, 3, buff->indexbuffer);
    add_index(1, 3, 4, buff->indexbuffer);

	return buff;
}

RBuffers* MeshGen::Hemisphere(float x, float y, float z, float R, unsigned int Color)
{
    float PI = 3.14159265f;
    float Radius = 1.0f;
    RBuffers* buff = new RBuffers;

    int  i, j;
    int p = 50; // Number of longitudinal slices.
    int q = 50; // Number of latitudinal slices.

    unsigned int cone = 0;
    for (j = 0; j < q; j++)
    {
        // One latitudinal triangle strip.
        for (i = 0; i <= p; i++)
        {
            add_vertex((R * cos((float)(j + 1) / q * PI / 2.0) * cos(2.0 * (float)i / p * PI)) + x,
                (R * sin((float)(j + 1) / q * PI / 2.0)) + y,
                (R * cos((float)(j + 1) / q * PI / 2.0f) * sin(2.0f * (float)i / p * PI)) + z, buff->vectorbuffer, buff->colorbuffer, Color);
            add_index(cone++, buff->indexbuffer);

            add_vertex((R * cos((float)j / q * PI / 2.0f) * cos(2.0f * (float)i / p * PI)) + x,
                (R * sin((float)j / q * PI / 2.0f)) + y,
                (R * cos((float)j / q * PI / 2.0f) * sin(2.0f * (float)i / p * PI)) + z, buff->vectorbuffer, buff->colorbuffer, Color);
            add_index(cone++, buff->indexbuffer);

        }
    }
    return buff;
}
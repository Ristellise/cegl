#ifndef CAMERA_H
#define CAMERA_H

#include "Vector3.h"
#include <Mtx44.h>
class Camera
{
public:
	Vector3 position;
	Vector3 target;
	Vector3 up;

	Camera();
	~Camera();
	void Init(const Vector3& pos, const Vector3& target, const Vector3& up);
	void Reset();
	void Update(double dt);
    void doUpdate(double dt);
    Mtx44 LookUpdate(float eyeX, float eyeY, float eyeZ,
        float centerX, float centerY, float centerZ,
        float upX, float upY, float upZ);

    Vector3 WorldUp = Vector3(0.0f, 1.0f, 0.0f);
    Vector3 Direction;
    float yaw = 0.1f;
    float pitch = 0.1f;

    Vector3 Position, Target, Up;
    Vector3 Right, Front;
};

#endif
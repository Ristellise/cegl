#ifndef MESH_BUILDER_H
#define MESH_BUILDER_H

#include "Mesh.h"
#include <vector>

/******************************************************************************/
/*!
		Class MeshBuilder:
\brief	Provides methods to generate mesh of different shapes
*/
/******************************************************************************/
class MeshBuilder
{
public:
	static Mesh* GenerateAxes(const std::string &meshName, float lengthX, float lengthY, float lengthZ);
	static Mesh* GenerateQuad(const std::string &meshName, float lengthX, float lengthY);
	static Mesh* GenerateCube(const std::string &meshName, float lengthX, float lengthY, float lengthZ);
	
	static Mesh* GenerateTorus(const std::string &meshName, unsigned int numStack, unsigned int numSlice, float outerR, float innerR, unsigned long int color, float slice = 360.0f, float stack = 360.0f);
	static Mesh* PolyBuilder(const std::string & meshName, float x, float y, float z, float R, int Sides=50, unsigned int InnerColor=0xffffff, unsigned int OuterColor = 0xffffff);
    static Mesh* Ring(const std::string &meshName, float RingWidth, float RingRadius, float x, float y, float z);
    static Mesh* Hemisphere(const std::string &meshName, float x, float y, float z, float R = 1.0f, unsigned int Color = 0xffffff);
    static Mesh* Ellipsoid(const std::string &meshName, unsigned int uiStacks, unsigned int uiSlices, float fA, float fB, float fC,unsigned int Color);
	static Mesh* Octahedron(const std::string &meshName, float H, float R, unsigned int Color);
    static Mesh* GenerateCylinder(const std::string &meshName, unsigned numStack, unsigned numSlice, float radius, float height, unsigned int color);
    /* Time to Work on my Assignment I guess.*/
	
};

#endif
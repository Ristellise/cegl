
#include "Mesh.h"
#include "GL\glew.h"

/******************************************************************************/
/*!
\brief
Default constructor

\param meshName - name of mesh
*/
/******************************************************************************/
Mesh::Mesh(const std::string &meshName)
	: name(meshName)
	, mode(DRAW_TRIANGLES)
    , DrawMode(GL_TRIANGLES)
{
	printf("Creating buffers for %s", meshName.c_str());
    glGenBuffers(1, &vertexBuffer);
    glGenBuffers(1, &colorBuffer);
    glGenBuffers(1, &indexBuffer);
    
}

/******************************************************************************/
/*!
\brief
Extra constructor

\param meshName - name of mesh
\param GL_MODE - OpenGL DRawingModes. [GL_TRIANGLES etc...]
*/
/******************************************************************************/
Mesh::Mesh(const std::string &meshName,unsigned int GL_MODE)
    : name(meshName)
    , mode(DRAW_TRIANGLES)
    , DrawMode(GL_MODE)
{
    printf("Creating buffers for %s\n", meshName.c_str());
    glGenBuffers(1, &vertexBuffer);
    glGenBuffers(1, &colorBuffer);
    glGenBuffers(1, &indexBuffer);

}

/*-------------------------------
- Destroys the Mesh... Rather Violently.
IN/OUT:
 - N/A: This is a Destructor.
-------------------------------*/
Mesh::~Mesh()
{
	printf("Destroying buffers for %s...\n", name.c_str());
    glDeleteBuffers(1, &vertexBuffer);
    glDeleteBuffers(1, &colorBuffer);
	printf("Buffers Destryoed for %s.\n", name.c_str());
}

void Mesh::addInstruction(ExInstructionTypes Type,float Rot, float X, float Y, float Z)
{
    ExInstruction inst;
    inst.degree = Rot;
    inst.Instruction = Type;
    inst.X = X;
    inst.Y = Y;
    inst.Z = Z;
	this->MeshInstructionSet.push_back(inst);
}

void Mesh::addInstruction(ExInstructionTypes Type)
{
	ExInstruction inst;
	inst.Instruction = Type;
	this->MeshInstructionSet.push_back(inst);
}

void Mesh::addInstruction(ExInstructionTypes Type, std::string Meshname)
{
	ExInstruction inst;
	inst.Instruction = Type;
	inst.meshName = Meshname;
	this->MeshInstructionSet.push_back(inst);
}

Mesh* getMeshFromMeshlist(std::vector<Mesh*> &meshlist,std::string &name)
{
	for (size_t i = 0; i < meshlist.size(); i++)
	{
		if (meshlist[i]->name == name)
		{
			return meshlist[i];
		}
	}
	return nullptr;
}

std::vector<ExInstruction> Mesh::getMeshInstructions()
{
    return this->MeshInstructionSet;
}

void Mesh::ExecuteInstructions(MS &modelStack, Mtx44 MVP, MS projectionStack, MS viewStack,
							   unsigned int *m_parameters, std::vector<Mesh*> meshList, Light lights[])
{
	Mtx44 modelView, modelViewIvTrpose;
    for (size_t i = 0; i < this->MeshInstructionSet.size(); i++)
    {
        ExInstruction Exinst = this->MeshInstructionSet[i];
        
        if (Exinst.Instruction == ExInstructionTypes::PSHMATRX)
        {
            modelStack.PushMatrix();
        }
        else if (Exinst.Instruction == ExInstructionTypes::POPMTRX)
        {
            modelStack.PopMatrix();
        }
		else if (Exinst.Instruction == ExInstructionTypes::CLRMTRX)
		{
			modelStack.Clear();
		}
		else if (Exinst.Instruction == ExInstructionTypes::RENDEROTHER)
		{
			// Update Matrix
			MVP = projectionStack.Top() * viewStack.Top() * modelStack.Top();
			glUniformMatrix4fv(m_parameters[U_MVP], 1, GL_FALSE, &MVP.a[0]);
			modelView = viewStack.Top() * modelStack.Top();
			// God said "Let there be light"
			// and there was darkness, because he forgotten to include the colors of the world.
			if (this->useLight)
			{
				glUniform1i(m_parameters[U_NEEDSLIGHT], 1);
				modelViewIvTrpose = modelView.GetInverse().GetTranspose();
				glUniformMatrix4fv(m_parameters[U_MDL_INV_TRP], 1,GL_FALSE,&modelViewIvTrpose.a[0]);
				glUniform3fv(m_parameters[U_MAT_AMB], 1, &this->material.kAmbient.r);
				glUniform3fv(m_parameters[U_MAT_DIF], 1, &this->material.kDiffuse.r);
				glUniform3fv(m_parameters[U_MAT_SPC], 1, &this->material.kSpecular.r);
				glUniform1f(m_parameters[U_MAT_SHIT], this->material.kShininess);

			}
            else
            {
                glUniform1i(m_parameters[U_NEEDSLIGHT], 0);
            }
			Pos lightPosition_cameraspace = viewStack.Top() * lights[0].pos;
			glUniform3fv(m_parameters[U_LIT_POS], 1,
				&lightPosition_cameraspace.x);
			// get the Other named mesh. and renders it from the view point of the current mesh. 
			getMeshFromMeshlist(meshList, Exinst.meshName)->Render();
		}
        else if (Exinst.Instruction == ExInstructionTypes::RENDER)
        {
            glUniform1i(m_parameters[U_NEEDSLIGHT], 0);
			MVP = projectionStack.Top() * viewStack.Top() * modelStack.Top();
			glUniformMatrix4fv(m_parameters[U_MVP], 1, GL_FALSE, &MVP.a[0]);
            this->Render();
        }
        else if (Exinst.Instruction == ExInstructionTypes::TRANSLATE)
        {
            modelStack.Translate(Exinst.X, Exinst.Y, Exinst.Z);
        }
        else if (Exinst.Instruction == ExInstructionTypes::ROTATE)
        {
            modelStack.Rotate(Exinst.degree,Exinst.X, Exinst.Y, Exinst.Z);
        }
        else if (Exinst.Instruction == ExInstructionTypes::SCALE)
        {
            modelStack.Scale(Exinst.X, Exinst.Y, Exinst.Z);
        }
    }
}

/*-------------------------------
- Your ModelRender Function.
IN:
 - void: Because it uses Opengl to get the stuff.
OUT:
 - void: Because it only just draws it.
-------------------------------*/
void Mesh::Render()
{
    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);

    glEnableVertexAttribArray(1);
    glBindBuffer(GL_ARRAY_BUFFER, colorBuffer);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);
    glDrawElements(this->DrawMode, this->indexSize, GL_UNSIGNED_INT, 0);

	glEnableVertexAttribArray(2);
    glBindBuffer(GL_ARRAY_BUFFER, normalBuffer);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE,0 , 0);
	glDisableVertexAttribArray(2);
    glDisableVertexAttribArray(1);
    glDisableVertexAttribArray(0);
}
#pragma once
#ifndef UTIL
#define UTIL
#include <string>
#include <iostream>
#include <Mtx44.h>
#include <vector>
struct Pos
{
    float x = 0;
    float y = 0;
    float z = 0;
    Pos(float X = 0.1f, float Y = 0.1f, float Z = 0.1f)
    {
        this->x = X;
        this->y = Y;
        this->z = Z;
    }
    void Set(float x, float y, float z) { this->x = x; this->y = y; this->z = z; }
};

struct Color
{
    float r, g, b;
    Color(float r = 1, float g = 1, float b = 1) { Set(r, g, b); }
    void Set(float r, float g, float b) { this->r = r; this->g = g; this->b = b; }
};

struct Light
{
    Pos pos;
    Color color;
    float power;
    float kC, kL, kQ;

    Light() // this is constructor for Light
    {
        color.Set(1, 1, 1);
        power = 1.f;
        kC = 1.f;
        kL = 0.f;
        kQ = 0.f;
    }
};

struct Component
{
    float r, g, b;
    Component(float r = 0.1f, float g = 0.1f, float b = 0.1f)
    {
        Set(r, g, b);
    }
    void Set(float r, float g, float b)
    {
        this->r = r; this->g = g; this->b = b;
    }
};

struct Material
{
    Component kAmbient;
    Component kDiffuse;
    Component kSpecular;
    float kShininess;
    Material(
        float KAmbR = 0.1f, float KAmbG = 0.1f, float KAmbB = 0.1f,
        float KDifR = 0.1f, float KDifG = 0.1f, float KDifB = 0.1f,
        float KSpcR = 0.1f, float KSpcG = 0.1f, float KSpcB = 0.1f)
    {
        kAmbient.Set(KAmbR, KAmbG, KAmbB);
        kDiffuse.Set(KDifR, KDifG, KDifB);
        kSpecular.Set(KSpcR, KSpcG, KSpcB);
    }
};

Pos operator*(const Mtx44& lhs, const Pos& rhs);

#endif // !UTIL


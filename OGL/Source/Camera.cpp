#include "Camera.h"
#include "Application.h"
#include <GLFW/glfw3.h>

/*---------------------
- Enums for Looking and Direction
---------------------*/


/******************************************************************************/
/*!
\brief
Default constructor
*/
/******************************************************************************/
Camera::Camera()
{
}

/******************************************************************************/
/*!
\brief
Destructor
*/
/******************************************************************************/
Camera::~Camera()
{
}

/******************************************************************************/
/*!
\brief
Initialize camera

\param pos - position of camera
\param target - where the camera is looking at
\param up - up vector of camera
*/
/******************************************************************************/
void Camera::Init(const Vector3& pos, const Vector3& target, const Vector3& up)
{
    this->position = pos;
    this->target = target;
    this->up = up;
    this->yaw = 0.1f;
    this->pitch = 0.0f;
}

/******************************************************************************/
/*!
\brief
Reset the camera settings
*/
/******************************************************************************/
void Camera::Reset()
{
}

/******************************************************************************/
/*!
\brief
To be called every frame. Camera will get user inputs and update its position and orientation

\param dt - frame time
*/
/******************************************************************************/
void Camera::doUpdate(double dt)
{
    float velocity = 2.5f * (float)dt;
    if (Application::IsKeyPressed('W'))
    {
        position += Front * velocity;
    }
    if (Application::IsKeyPressed('S'))
    {
        position -= Front * velocity;
    }
    if (Application::IsKeyPressed('A'))
    {
        position -= Right * velocity;
    }
    if (Application::IsKeyPressed('D'))
    {
        position += Right * velocity;
    }
    if (Application::IsKeyPressed(VK_LCONTROL))
    {
        position += Up * velocity;
    }
    if (Application::IsKeyPressed(VK_LSHIFT))
    {
        position -= Up * velocity;
    }

    if (Application::IsKeyPressed(VK_UP))
    {
        pitch += 100 * (float)dt;
    }
    if (Application::IsKeyPressed(VK_DOWN))
    {
        pitch -= 100 * (float)dt;
    }
    if (Application::IsKeyPressed(VK_LEFT))
    {
        yaw -= 100 * (float)dt;
    }
    if (Application::IsKeyPressed(VK_RIGHT))
    {
        yaw += 100 * (float)dt;
    }
    Direction.x = cos(Math::DegreeToRadian(yaw)) * cos(Math::DegreeToRadian(pitch));
    Direction.y = sin(Math::DegreeToRadian(pitch));
    Direction.z = sin(Math::DegreeToRadian(yaw)) * cos(Math::DegreeToRadian(pitch));

    Front = Direction.Normalize();
    Right = Front.Cross(WorldUp).Normalize();
    Up = Right.Cross(Front);
    Up = Up.Normalize();
}
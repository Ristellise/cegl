#ifndef MESH_H
#define MESH_H


#include <MatrixStack.h>
#include "Utils.h"
#include <string>
#include <vector>
/******************************************************************************/
/*!
		Class Mesh:
\brief	To store VBO (vertex & color buffer) and IBO (index buffer)
*/
/******************************************************************************/

/*
Buffers. These are used to hold all the initalized/loaded vectors.
*/

enum ExInstructionTypes
{
    NULLINSTRUCTION,
    TRANSLATE,
    ROTATE,
    SCALE,
    RENDER,
    PSHMATRX,
    POPMTRX,
    CLRMTRX,
	RENDEROTHER,
	COMMENT,
    COUNT
};



struct ExInstruction
{
    ExInstructionTypes Instruction;
    float degree = 0;
    float X = 0;
    float Y = 0;
    float Z = 0;
	std::string meshName = "";
};

struct Buffers
{
    unsigned int DrawMode;
    unsigned vertexBuffer;
    unsigned colorBuffer;
    unsigned indexBuffer;
    unsigned normalBuffer;
    unsigned indexSize;
};

// We bring this up to Mesh.h because I need to referance it in Mesh.cpp.
enum UNIFORM_TYPE
{
	U_MVP = 0,
	U_MV,
	// Needs light
	U_NEEDSLIGHT,
	U_MDL_INV_TRP,
	// Material
	U_MAT_AMB,
	U_MAT_DIF,
	U_MAT_SPC,
	U_MAT_SHIT,
	// LIGHT 
	U_LIT_POS,
	U_LIT_COL,
	U_LIT_POW,
	U_LIT_KC,
	U_LIT_KL,
	U_LIT_KQ,
	U_LIT_ENABLE,

	U_TOTAL
};

class Mesh
{
public:
	enum DRAW_MODE
	{
		DRAW_TRIANGLES, //default mode
		DRAW_TRIANGLE_STRIP,
		DRAW_LINES,
		DRAW_MODE_LAST,
	};
	Mesh(const std::string &meshName);
    Mesh(const std::string &meshName, unsigned int GL_MODE);
	~Mesh();
	void Render();
    void Render(MS ModelStack);

	const std::string name;
	DRAW_MODE mode;
    unsigned int DrawMode;
	unsigned vertexBuffer;
	unsigned normalBuffer;
	unsigned colorBuffer;
	unsigned indexBuffer;
	unsigned indexSize;
	Material material;
	bool useLight = false;
    void ExecuteInstructions(
        MS &modelStack, Mtx44 MVP, MS projectionStack, MS viewStack,
        unsigned int *m_parameters, std::vector<Mesh*> meshList, Light lights[]);
    void addInstruction(ExInstructionTypes Type, float Rot, float X, float Y, float Z);
	void addInstruction(ExInstructionTypes Type, std::string meshName);
	void addInstruction(ExInstructionTypes Type);
    std::vector<ExInstruction> getMeshInstructions();
private:
    std::vector<ExInstruction> MeshInstructionSet;
    std::vector<Buffers> Buffers;
};

#endif
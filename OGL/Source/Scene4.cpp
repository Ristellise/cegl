#include "Scene4.h"
#include "GL\glew.h"
#include <cstdio>

#include "shader.hpp"
#include "Application.h"

Scene4::Scene4()
{}

Scene4::~Scene4()
{
}

Mesh* getMeshByName(const std::string &name, std::vector<Mesh*> meshList)
{
    for (size_t i = 0; i < meshList.size(); i++)
    {
        if (meshList[i]->name == name)
        {
            return meshList[i];
        }
    }
    return nullptr;
}

void coutspace(unsigned int spacecount)
{
	for (size_t i = 0; i < spacecount; i++)
	{
		std::cout << " ";
	}
}

// Prints out the meshList in a tree format
void printMeshMap(std::vector<Mesh*> meshList)
{
	unsigned int spacing = 0;
	std::vector<ExInstruction> inst;
	for (size_t i = 0; i < meshList.size(); i++)
	{
		inst = meshList[i]->getMeshInstructions();
		for (size_t i2 = 0; i2 < inst.size(); i2++)
		{
			if (inst[i2].Instruction == ExInstructionTypes::CLRMTRX)
			{
				spacing = 0;
				coutspace(spacing);
				std::cout << (char)219 << "CLR_MTX" << std::endl;
				
			}
			else if (inst[i2].Instruction == ExInstructionTypes::PSHMATRX)
			{
				//spacing++;
				coutspace(spacing);
				std::cout << (char)192 << "PUSH_MTX" << std::endl;
				spacing++;
				
			}
			else if (inst[i2].Instruction == ExInstructionTypes::POPMTRX)
			{
				//spacing--;
				coutspace(spacing);
				std::cout << (char)195 << "POP_MTX" << std::endl;
				spacing--;
			}
			else
			{
				coutspace(spacing);
				std::cout << (char)195;
				if (inst[i2].Instruction == ExInstructionTypes::RENDER)
				{
					std::cout << "RENDER" << std::endl;
				}
				else if (inst[i2].Instruction == ExInstructionTypes::RENDEROTHER)
				{
					std::cout << "RENDEO\tMESHNAME: " << inst[i2].meshName.c_str() << std::endl;
				}
				else if (inst[i2].Instruction == ExInstructionTypes::TRANSLATE)
				{
					std::cout << "TRANTE\tX:"<< inst[i2].X << " | Y:"<< inst[i2].Y << " | Z:" << inst[i2].Z << std::endl;
				}
				else if (inst[i2].Instruction == ExInstructionTypes::ROTATE)
				{
					std::cout << "ROTATE\tX:" << inst[i2].X << " | Y:" << inst[i2].Y<< 
								 " | Z:" << inst[i2].Z << " | ROT:" << inst[i2].degree << std::endl;
				}
				else if (inst[i2].Instruction == ExInstructionTypes::SCALE)
				{
					std::cout << "SCALEM\tX:" << inst[i2].X << " | Y:" << inst[i2].Y << " | Z:" << inst[i2].Z << std::endl;
				}
				else if (inst[i2].Instruction == ExInstructionTypes::COMMENT)
				{
					std::cout << "COMMENT:\t" << inst[i2].meshName << std::endl;
				}
				else
				{
					std::cout << "UNKNOWN " << std::endl;
				}
			}
		}
	}
}

void Scene4::Init()
{
	// Init VBO here
	glClearColor(0.0f, 0.0f, 0.4f, 0.0f);
    m_progID = LoadShaders("Shader//Shading.vertexshader",
		"Shader//Shading.fragmentshader");
    // Use our shader

	m_parameters[U_MV] = glGetUniformLocation(m_progID, "MV");
	m_parameters[U_MDL_INV_TRP] =
		glGetUniformLocation(m_progID, "MV_inverse_transpose");
	m_parameters[U_MAT_AMB] = glGetUniformLocation(m_progID,
		"material.kAmbient");
	m_parameters[U_MAT_DIF] = glGetUniformLocation(m_progID,
		"material.kDiffuse");
	m_parameters[U_MAT_SPC] = glGetUniformLocation(m_progID,
		"material.kSpecular");
	m_parameters[U_MAT_SHIT] = glGetUniformLocation(m_progID,
		"material.kShininess");
	m_parameters[U_LIT_POS] = glGetUniformLocation(m_progID,
		"lights[0].position_cameraspace");
	m_parameters[U_LIT_COL] = glGetUniformLocation(m_progID, "lights[0].color");
	m_parameters[U_LIT_POW] = glGetUniformLocation(m_progID,
		"lights[0].power");
	m_parameters[U_LIT_KC] = glGetUniformLocation(m_progID, "lights[0].kC");
	m_parameters[U_LIT_KL] = glGetUniformLocation(m_progID, "lights[0].kL");
	m_parameters[U_LIT_KQ] = glGetUniformLocation(m_progID, "lights[0].kQ");
	m_parameters[U_LIT_ENABLE] = glGetUniformLocation(m_progID, "lightEnabled");

    glUseProgram(m_progID);
    // Generate default VAO
    glGenVertexArrays(1, &m_vertexArrayID);
    glBindVertexArray(m_vertexArrayID);

    printf("OK.\n");
    printf("Cam Init... ");
    cam.Init(Vector3(1, 0, 0), Vector3(0, 0, 0), Vector3(0, 1, 0));
    printf("OK. Init Done.\n");
	// Axes Mesh
	Mesh* workingmesh = MeshBuilder::GenerateAxes("This.Axes", 1.0f, 0.5f, 0.0f);
	workingmesh->addInstruction(ExInstructionTypes::RENDER);
	meshList.push_back(workingmesh);

	// HairMesh [All the Octagons.] [1 Mesh]
	workingmesh = MeshBuilder::Octahedron("HairOctagons", 0.25f, 0.1f, 0x423A37);
	for (size_t i = 0; i < 4; i++)
	{
		workingmesh->addInstruction(ExInstructionTypes::PSHMATRX);
		workingmesh->addInstruction(ExInstructionTypes::ROTATE, 90, 1.0f, 0.0f, 0.0f);
		workingmesh->addInstruction(ExInstructionTypes::ROTATE, i*10, 0.0f, 0.0f, 1.0f);
		workingmesh->addInstruction(ExInstructionTypes::ROTATE, i * 10, 0.0f, 1.0f, 0.0f);
		workingmesh->addInstruction(ExInstructionTypes::RENDER);
		workingmesh->addInstruction(ExInstructionTypes::PSHMATRX);
		workingmesh->addInstruction(ExInstructionTypes::TRANSLATE, 0.0f, 0.0f, -0.2f, 0.0f);
		workingmesh->addInstruction(ExInstructionTypes::ROTATE, -25, 1.0f, 0.0f, 0.0f);
		workingmesh->addInstruction(ExInstructionTypes::RENDER);
		workingmesh->addInstruction(ExInstructionTypes::PSHMATRX);
		workingmesh->addInstruction(ExInstructionTypes::TRANSLATE, 0.0f, 0.0f, -0.2f, 0.0f);
		workingmesh->addInstruction(ExInstructionTypes::ROTATE, -25, 1.0f, 0.0f, 0.0f);
		workingmesh->addInstruction(ExInstructionTypes::RENDER);
		workingmesh->addInstruction(ExInstructionTypes::PSHMATRX);
		workingmesh->addInstruction(ExInstructionTypes::TRANSLATE, 0.0f, 0.0f, -0.2f, 0.0f);
		workingmesh->addInstruction(ExInstructionTypes::ROTATE, -25, 1.0f, 0.0f, 0.0f);
		workingmesh->addInstruction(ExInstructionTypes::RENDER);
		workingmesh->addInstruction(ExInstructionTypes::PSHMATRX);
		workingmesh->addInstruction(ExInstructionTypes::TRANSLATE, 0.0f, 0.0f, -0.2f, 0.0f);
		workingmesh->addInstruction(ExInstructionTypes::ROTATE, -25, 1.0f, 0.0f, 0.0f);
		workingmesh->addInstruction(ExInstructionTypes::RENDER);
		workingmesh->addInstruction(ExInstructionTypes::CLRMTRX);
	}
	workingmesh->material = Material();
	meshList.push_back(workingmesh);
	// HairMesh [1 Mesh]
	workingmesh = MeshBuilder::Hemisphere("HairBase", 0.0f, 0.0f, 0.0f, 0.5f, 0x423A37);
	workingmesh->material = Material();

	workingmesh->addInstruction(ExInstructionTypes::PSHMATRX);
	workingmesh->addInstruction(ExInstructionTypes::TRANSLATE, 0.0f, 0.0f, -0.4f, 0.0f);
	workingmesh->addInstruction(ExInstructionTypes::RENDER);

	
	meshList.push_back(workingmesh);

	// FaceMesh [1 Mesh]
	workingmesh = MeshBuilder::Ellipsoid("FaceBase",50,50,0.4f,0.5f,0.4f, 0xFDD6B6);
	workingmesh->addInstruction(ExInstructionTypes::TRANSLATE, 0.0f, 0.0f, 0.0f, 0.0f);
	workingmesh->addInstruction(ExInstructionTypes::RENDER);
	//Neck
	workingmesh->addInstruction(ExInstructionTypes::TRANSLATE, 0.0f, 0.0f, -0.5f, 0.0f);
    workingmesh->addInstruction(ExInstructionTypes::PSHMATRX);
	workingmesh->addInstruction(ExInstructionTypes::SCALE, 0.4f, 0.25f, 0.4f, 0.25f);
	workingmesh->addInstruction(ExInstructionTypes::RENDER);
    workingmesh->addInstruction(ExInstructionTypes::POPMTRX);
	meshList.push_back(workingmesh);
	
	// BodyMesh [1 Mesh]
	workingmesh = MeshBuilder::Ellipsoid("BdyEllip", 50, 50, 0.25, 0.25, 0.25, 0x423A37);
	workingmesh->addInstruction(ExInstructionTypes::TRANSLATE, 0.0f, 0.0f, -0.3f, 0.0f);
	workingmesh->addInstruction(ExInstructionTypes::RENDER);
	meshList.push_back(workingmesh);

	//Shoulders [1 Mesh]
	workingmesh = MeshBuilder::GenerateTorus("BdyHalfTor",50,50,0.5f, 0.25f, 0x423A37,135,360);
	workingmesh->addInstruction(ExInstructionTypes::TRANSLATE, 0.0f, 0.0f, -0.6f, 0.0f);
    workingmesh->addInstruction(ExInstructionTypes::PSHMATRX);
    workingmesh->addInstruction(ExInstructionTypes::ROTATE, 22.5, 0.0f, 0.0f, 1.0f);
	workingmesh->addInstruction(ExInstructionTypes::RENDER);
    workingmesh->addInstruction(ExInstructionTypes::POPMTRX);
    meshList.push_back(workingmesh);
    //Body [Cylinders] [1 Mesh]
    workingmesh = MeshBuilder::GenerateCylinder("BdyCyl",50,50,0.25f,0.25f, 0xFDD6B6);
	workingmesh->addInstruction(ExInstructionTypes::COMMENT, "Core Body");
	workingmesh->addInstruction(ExInstructionTypes::PSHMATRX);
	// Scale up
    workingmesh->addInstruction(ExInstructionTypes::SCALE, 0, 1.0f, 5.0f, 1.0f);
	// Render left
    workingmesh->addInstruction(ExInstructionTypes::PSHMATRX);
    workingmesh->addInstruction(ExInstructionTypes::TRANSLATE, 0.0f, -0.43f, -0.049f, 0.0f);
    workingmesh->addInstruction(ExInstructionTypes::ROTATE, 5, 0.0f, 0.0f, 1.0f);
    workingmesh->addInstruction(ExInstructionTypes::RENDER);
    workingmesh->addInstruction(ExInstructionTypes::POPMTRX);
	// Render Right
    workingmesh->addInstruction(ExInstructionTypes::PSHMATRX);
    workingmesh->addInstruction(ExInstructionTypes::TRANSLATE, 0.0f, 0.43f, -0.049f, 0.0f);
    workingmesh->addInstruction(ExInstructionTypes::ROTATE, -5, 0.0f, 0.0f, 1.0f);
    workingmesh->addInstruction(ExInstructionTypes::RENDER);
    workingmesh->addInstruction(ExInstructionTypes::POPMTRX);
	// Render Center
	workingmesh->addInstruction(ExInstructionTypes::RENDER);
	workingmesh->addInstruction(ExInstructionTypes::POPMTRX);

	workingmesh->addInstruction(ExInstructionTypes::COMMENT,"Core Body Spheres");
	workingmesh->addInstruction(ExInstructionTypes::PSHMATRX);
	workingmesh->addInstruction(ExInstructionTypes::SCALE, 0, 2.0, 2, 0.6f);
	workingmesh->addInstruction(ExInstructionTypes::TRANSLATE, 0.0f, 0.055f, 0.0f, 0.27f);
	workingmesh->addInstruction(ExInstructionTypes::RENDEROTHER, "BdyEllip");
	workingmesh->addInstruction(ExInstructionTypes::POPMTRX);
	workingmesh->addInstruction(ExInstructionTypes::PSHMATRX);
	workingmesh->addInstruction(ExInstructionTypes::SCALE, 0, 2.0, 2, 0.6f);
	workingmesh->addInstruction(ExInstructionTypes::TRANSLATE, 0.0f, -0.055f, 0.0f, 0.27f);
	workingmesh->addInstruction(ExInstructionTypes::RENDEROTHER, "BdyEllip");
	workingmesh->addInstruction(ExInstructionTypes::POPMTRX);
	workingmesh->addInstruction(ExInstructionTypes::PSHMATRX);
	workingmesh->addInstruction(ExInstructionTypes::SCALE, 0, 2.0, 2, 0.6f);
	workingmesh->addInstruction(ExInstructionTypes::TRANSLATE, 0.0f, 0, 0.05f, 0.27f);
	workingmesh->addInstruction(ExInstructionTypes::RENDEROTHER, "BdyEllip");
	workingmesh->addInstruction(ExInstructionTypes::POPMTRX);

	workingmesh->addInstruction(ExInstructionTypes::COMMENT, "Chest Sizing... Okay I swear this is not lewd.");
	workingmesh->addInstruction(ExInstructionTypes::PSHMATRX);
	workingmesh->addInstruction(ExInstructionTypes::SCALE, 0, 1.0, 2, 1.5f);
	workingmesh->addInstruction(ExInstructionTypes::TRANSLATE, 0.0f, 0.2f, 0.02f, 0.05f);
	workingmesh->addInstruction(ExInstructionTypes::RENDEROTHER, "BdyEllip");
	workingmesh->addInstruction(ExInstructionTypes::POPMTRX);
	workingmesh->addInstruction(ExInstructionTypes::PSHMATRX);
	workingmesh->addInstruction(ExInstructionTypes::SCALE, 0, 1.0, 2, 1.5f);
	workingmesh->addInstruction(ExInstructionTypes::TRANSLATE, 0.0f, -0.2f, 0.02f, 0.05f);
	workingmesh->addInstruction(ExInstructionTypes::RENDEROTHER, "BdyEllip");
	workingmesh->addInstruction(ExInstructionTypes::POPMTRX);

	workingmesh->addInstruction(ExInstructionTypes::COMMENT, "Lower Body Section");
	workingmesh->addInstruction(ExInstructionTypes::PSHMATRX);
	workingmesh->addInstruction(ExInstructionTypes::SCALE, 0, 2.0, 1.0f, 1.0f);
	workingmesh->addInstruction(ExInstructionTypes::TRANSLATE, 0.0f, 0, 0.5f, 0);
	workingmesh->addInstruction(ExInstructionTypes::RENDEROTHER, "BdyEllip");

	workingmesh->addInstruction(ExInstructionTypes::CLRMTRX);
	meshList.push_back(workingmesh);
    std::cout << "Final MeshCount: " << meshList.size() << std::endl;
	std::cout << "MESH TREE----------------------------------------" << std::endl;
	printMeshMap(meshList);
	std::cout << "FINISHED ----------------------------------------" << std::endl;
	glEnable(GL_DEPTH_TEST);
    projection.SetToPerspective(45.f, 4.f / 3.f, 0.1f, 1000.f);
    projectionStack.LoadMatrix(projection);

    lights[0].pos.Set(0, 10, 0);
    lights[0].color.Set(1, 1, 1);
    lights[0].power = 1;
    lights[0].kC = 1.f;
    lights[0].kL = 0.01f;
    lights[0].kQ = 0.001f;
}

void Scene4::Update(double dt)
{
    cam.doUpdate(dt);
    if (!glIsEnabled(GL_CULL_FACE) && Application::IsKeyPressed(VK_NUMPAD1))
    {
        printf("Enabled Face Culling.\n");
        glEnable(GL_CULL_FACE);
    }
    else if (glIsEnabled(GL_CULL_FACE) && Application::IsKeyPressed(VK_NUMPAD2))
    {
        printf("Disabled Face Culling.\n");
        glDisable(GL_CULL_FACE);
    }
    glGetIntegerv(GL_POLYGON_MODE, &polygonMode);
    if (polygonMode ==  GL_FILL && Application::IsKeyPressed(VK_NUMPAD4))
    {
        printf("Using Line mode.\n");
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    }
    else if (polygonMode == GL_LINE && Application::IsKeyPressed(VK_NUMPAD3))
    {
        printf("Using Fill mode.\n");
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    }
}

void Scene4::Render()
{	// Render VBO here
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    // Set All to IndentityMatrixes
    viewStack.LoadIdentity();
    viewStack.LookAt(cam.position.x, cam.position.y, cam.position.z,
        cam.position.x + cam.Front.x, cam.position.y + cam.Front.y, cam.position.z + cam.Front.z,
        cam.Up.x, cam.Up.y, cam.Up.z);
    modelStack.LoadIdentity();

    for (size_t i = 0; i < meshList.size(); i++)
    {
		meshList[i]->ExecuteInstructions(modelStack,MVP,projectionStack,viewStack,m_parameters,meshList,this->lights);
    }
}

void Scene4::Exit()
{
	// Cleanup VBO here
	glDeleteVertexArrays(1, &m_vertexArrayID);
	glDeleteProgram(m_progID);
}

#ifndef SCENE_4_H
#define SCENE_4_H

#include "Scene.h"
#include "Camera.h"
#include <Mtx44.h>
#include "Mesh.h"
#include "MeshBuilder.h"
#include <MatrixStack.h>
#include <vector>

class Scene4 : public Scene
{
public:
	Scene4();
	~Scene4();

	virtual void Init();
	virtual void Update(double dt);
	virtual void Render();
	virtual void Exit();

	enum GEOMETRY_TYPE
	{
		GEO_AXES,
		GEO_QUAD,
		GEO_CUBE,
		NUM_GEOMETRY,
	};
private:
	unsigned m_vertexArrayID;
	unsigned m_progID;
    int polygonMode;
	unsigned m_parameters[U_TOTAL];
	float rotateAngle =0.0f ;
	float rotateX = 0.0f;
	float rotateY = 0.0f;
	float rotateZ = 1.0f;
	float translateX = 0.0f;
	float translateY = 0.0f;
	float scaleAll = 0.0f;
    Camera cam;
	std::vector<Mesh*> meshList;
    //Matrixes
    Mtx44 translate, rotate, scale;
    Mtx44 model;
    Mtx44 view;
    Mtx44 projection;
    Mtx44 MVP;
    MS modelStack, viewStack, projectionStack;
    Light lights[1];
};

#endif
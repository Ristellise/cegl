#pragma once

#ifndef MESHGEN
#define MESHGEN
#include <vector>
#include "Utils.h"
struct RBuffers
{
    std::vector<float> vectorbuffer = {};
    std::vector<float> colorbuffer = {};
    std::vector<unsigned int> indexbuffer = {};
    std::vector<float> normalbuffer = {};
    //std::vector<unsigned int> UVbuffer = {};
};
static std::vector<float> dummyffs;
void add_vertex(float x, float y, float z, std::vector<float> &vertvector, std::vector<float> &colorvector, unsigned long int hexcolor = 0x000000,
    bool addNormal = false, std::vector<float> &normvector = dummyffs);
void add_index(unsigned int vectice1, unsigned int vectice2, unsigned int vectice3, std::vector<unsigned int>& indexBuffer);
void add_index(unsigned int vectice1, std::vector<unsigned int>& indexBuffer);
class MeshGen
{
public:
    static RBuffers* MultiPoly(float x, float y, float z, float R,
        int Sides,
        unsigned int InnerColor, unsigned int OuterColor);
    static RBuffers* Hemisphere(float x, float y, float z, float R, unsigned int Color);
    static RBuffers* Octahedron(float height, float radius, unsigned int Color);
};
#endif // !Z

